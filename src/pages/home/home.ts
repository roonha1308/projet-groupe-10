import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailsPhotosPage } from '../details-photos/details-photos';
import { FormPhotosPage } from '../form-photos/form-photos';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  detailsPhotos(){
    this.navCtrl.push(DetailsPhotosPage);
  }

  formPhotos(){
    this.navCtrl.push(FormPhotosPage);
  }

}
