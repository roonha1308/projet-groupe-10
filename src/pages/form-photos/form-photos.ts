import { Component } from '@angular/core';
import {  NavController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, OutputType, ImagePickerOptions } from '@ionic-native/image-picker';


/**
 * Generated class for the AppareilFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-form-photos',
  templateUrl: 'form-photos.html',
})
export class FormPhotosPage {
  mesPhotos: any;
  

  constructor(
              private navCtrl:  NavController,
              private camera: Camera,
              private imagePicker: ImagePicker
              ) {}

  prendrePhotos(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });
  } 
  
  galeriePhotos(){
    let options: ImagePickerOptions = {
      maximumImagesCount: 4,
      outputType: OutputType.FILE_URL
    }
    this.imagePicker.getPictures(options)
    .then((results)=>{
      console.log('img= ', results);
      this.mesPhotos.pictures = results;
    })
    .catch((err)=> console.log("erreur: ", err))
  }

 
}

